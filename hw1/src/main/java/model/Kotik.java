package model;

public class Kotik {

    private int satiety = 20;
    private static int count;
    private int prettiness;
    private String name;
    private int weight;
    private String meow;

    public Kotik() {
        count++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        count++;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public static int getCount() {
        System.out.print("Number of cats: ");
        return  count;
    }

    public boolean play() {
        if (satiety > 0) {
            satiety -= 5;
            return true;
        }
        return false;
    }

    public boolean sleep() {
        if (satiety > 0) {
            satiety -= 1;
            return true;
        }
        return false;
    }

    public boolean chaseMouse() {
        if (satiety > 0) {
            satiety -= 10;
            return true;
        }
        return false;
    }

    public boolean breakVase() {
        if (satiety > 0) {
            satiety -= 2;
            return true;
        }
        return false;
    }

    public boolean sharpenClaws() {
        if (satiety > 0) {
            satiety -= 3;
            return true;
        }
        return false;
    }

    public static void liveAnotherDay(Kotik name) {
        for (int i = 0; i < 24; i++) {

            switch ((int) (Math.random()*5 + 1)) {
            case (1):
                if (name.play()) {
                    System.out.println("play");
                }
                else {
                    name.eat(20, "fish");
                }
                break;
            case (2):
                if (name.sleep()) {
                    System.out.println("sleep");
                }
                else {
                    name.eat();
                }
                break;
            case (3):
                if (name.chaseMouse()) {
                    System.out.println("chaseMouse");
                }
                else {
                    name.eat();
                }
                break;
            case (4):
                if (name.breakVase()) {
                    System.out.println("breakVase");
                }
                else {
                    name.eat(20);
                }
                break;
            case (5):
                if (name.sharpenClaws()) {
                    System.out.println("sharpenClaws");
                }
                else {
                    name.eat(20);
                }
                break;
                }
            }
        }

    public void eat(int food) {
        System.out.println("eat");
        satiety += food;
    }

    public void eat(int food, String nameFood) {
        System.out.println("eat: " + nameFood);
        satiety += food;
    }

    public void eat() {
        eat(20, "milk");
    }
}
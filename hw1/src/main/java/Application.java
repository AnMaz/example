import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik murzik = new Kotik();
        murzik.setKotik(25, "Murzik",3500, "Meow");

        Kotik barsik = new Kotik(21, "Barsik", 2300, "meow");

        System.out.println("Variables \"meow\" are equal: " + murzik.getMeow().equals(barsik.getMeow()));

        Kotik.liveAnotherDay(murzik);

        System.out.println("Cat name: " + murzik.getName() + ". Its weight: " + murzik.getWeight() + "gr.");
        System.out.println (Kotik.getCount());
    }
}

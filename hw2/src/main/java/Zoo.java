import animals.*;
import food.*;

public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker();

        Food grass = new Grass();
        Food meat = new Meat();

        Monkey monkey = new Monkey("monkey");
        Tiger tiger = new Tiger("tiger");
        Beaver beaver1 = new Beaver("beaver Norbert");
        Beaver beaver2 = new Beaver("beaver Daggett");
        Fish fish1 = new Fish("fish Nemo");
        Fish fish2 = new Fish("fish Demo");
        Fish fish3 = new Fish("fish Memo");
        Duck duck = new Duck("duck");

        Swim[] pond = new Swim[] {fish1, beaver1, fish2, duck, beaver2, fish3};

        for (Swim animal : pond) {
            Animal obj = (Animal)animal;
            System.out.print("The " + obj.getAnimalName() + " performed an action: ");
            animal.swim();
        }

        System.out.println();
        monkey.eat(meat);

        System.out.println();
        monkey.eat(grass);

        System.out.println();
        //Если передать Fish, программа не скомпилируется
        System.out.println("The " + duck.getAnimalName() + " says: " + worker.getVoice(duck));

        System.out.println();
        System.out.println("Satiety " + tiger.getAnimalName() + " before feeding: " + tiger.getSatiety());
        worker.feed(tiger, grass);
        System.out.println("Satiety " + tiger.getAnimalName() + " after feeding: " + tiger.getSatiety());

        System.out.println();
        System.out.println("Satiety " + monkey.getAnimalName() + " before feeding: " + monkey.getSatiety());
        worker.feed(monkey, grass);
        System.out.println("Satiety " + monkey.getAnimalName() + " after feeding: " + monkey.getSatiety());
    }
}

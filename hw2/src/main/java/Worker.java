import animals.Animal;
import animals.Voice;
import food.Food;

public class Worker {

    public void feed(Animal animal, Food food) {
        if (animal.eat(food)) {
            System.out.println(("The " + animal.getAnimalName() + " has eaten " + food.getFoodName()) + "!");
        }
        else {
            System.out.println(("The " + animal.getAnimalName() + " hasn't eaten " + food.getFoodName()) + "!");
        }
    }


    public String getVoice(Voice animal) {
        return animal.voice();
    }
}


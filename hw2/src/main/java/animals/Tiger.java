package animals;

public class Tiger extends Carnivorous implements Voice, Run {

    public Tiger (String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("run");
    }

    @Override
    public String voice() {
        return "RAR RAR";
    }
}
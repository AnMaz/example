package animals;

import food.Food;
import food.Grass;

public abstract class Herbivor extends Animal {

    @Override
    public boolean eat(Food food) {
        if (food instanceof Grass) {
            setSatiety(food);
            System.out.println("This food is suitable!");
            return true;
        }
        else {
            System.out.println("The herbivore doesn't eat meat!");
            return false;
        }
    }
}

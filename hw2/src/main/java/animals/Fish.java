package animals;

public class Fish extends Carnivorous implements Swim {

    public Fish (String name) {
        this.name = name;
    }

    @Override
    public void swim() {
        System.out.println("swim");
    }
}

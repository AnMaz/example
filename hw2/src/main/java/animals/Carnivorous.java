package animals;

import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {

    @Override
    public boolean eat(Food food) {
        if (food instanceof Meat) {
            setSatiety(food);
            System.out.println("This food is suitable!");
            return true;
        }
        else {
            System.out.println("The carnivorous doesn't eat grass!");
            return false;
        }
    }
}


package animals;

import food.Food;

public abstract class Animal {

    private int satiety = 0;
    protected String name;

    protected void setSatiety(Food food) {
        satiety += food.theNutritionalValue();
    }

    public int getSatiety() {
        return satiety;
    }

    public abstract boolean eat(Food food);

    public String getAnimalName() {
        return name;
    }
}

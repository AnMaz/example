--Задание: 1 (Serge I: 2002-09-30)
--Найдите номер модели, скорость и размер жесткого диска для всех ПК стоимостью менее 500 дол. Вывести: model, speed и hd 
SELECT p.model,
       p.speed,
	   p.hd 
FROM pc p
WHERE p.price < 500


--Задание: 2 (Serge I: 2002-09-21)
--Найдите производителей принтеров. Вывести: maker
SELECT DISTINCT p.maker
FROM product p
WHERE p.type = 'printer'


--Задание: 3 (Serge I: 2002-09-30)
--Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол.
SELECT l.model,
       l.ram,
	   l.screen
FROM laptop l
WHERE l.price > 1000


--Задание: 4 (Serge I: 2002-09-21)
--Найдите все записи таблицы Printer для цветных принтеров. 
SELECT *
FROM printer p
WHERE p.color = 'y'


--Задание: 5 (Serge I: 2002-09-30)
--Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 12x или 24x CD и цену менее 600 дол. 
SELECT p.model,
       p.speed,
	   p.hd
FROM pc p
WHERE ( p.cd = '12x'
      OR p.cd = '24x')
AND p.price < 600


--Задание: 6 (Serge I: 2002-10-28)
--Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт,
--найти скорости таких ПК-блокнотов. Вывод: производитель, скорость. 
SELECT DISTINCT p.maker, l.speed
FROM laptop l
JOIN product p
ON p.model = l.model
WHERE l.hd >= 10


--Задание: 7 (Serge I: 2002-11-02)
--Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).
SELECT p.model, l.price
	FROM product p
	JOIN laptop l
	ON p.model = l.model
	WHERE p.maker = 'B'
UNION
SELECT p.model, pc.price
	FROM product p
	JOIN pc
	ON p.model = pc.model
	WHERE p.maker = 'B'
UNION
SELECT p.model, pr.price
	FROM product p
	JOIN printer pr
	ON p.model = pr.model
	WHERE p.maker = 'B'
	

--Задание: 8 (Serge I: 2003-02-03)
--Найдите производителя, выпускающего ПК, но не ПК-блокноты.
SELECT p.maker
	FROM product p
	WHERE p.type = 'PC'
EXCEPT
SELECT pr.maker
	FROM product pr
	WHERE pr.type = 'Laptop'


--Задание: 9 (Serge I: 2002-11-02)
--Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker
SELECT DISTINCT p.maker
	FROM product p
	JOIN pc
	ON p.model = pc.model
	WHERE pc.speed >= 450


--Задание: 10 (Serge I: 2002-09-23)
--Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price 
SELECT p.model, p.price
	FROM printer p
	WHERE p.price = (SELECT MAX(p.price)
                     FROM printer p)


--Задание: 11 (Serge I: 2002-11-02)
--Найдите среднюю скорость ПК.
SELECT SUM(pc.speed)/COUNT(*) AS average_speed 
FROM pc


--Задание: 12 (Serge I: 2002-11-02)
--Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол.
SELECT SUM(l.speed)/COUNT(*) AS average_speed
FROM laptop l
WHERE l.price > 1000


--Задание: 13 (Serge I: 2002-11-02)
--Найдите среднюю скорость ПК, выпущенных производителем A. 
SELECT AVG(pc.speed) AS average_speed
FROM product p
JOIN pc
ON p.model = pc.model
WHERE p.maker = 'A'


--Задание: 14 (Serge I: 2002-11-05)
--Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий. 
SELECT s.class,
       s.name,
	   c.country
FROM ships s
JOIN classes c
ON s.class = c.class
WHERE c.numguns >= 10


--Задание: 15 (Serge I: 2003-02-03)
--Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD
SELECT pc.hd
FROM pc
GROUP BY pc.hd
HAVING COUNT(*) > 1


--Задание: 16 (Serge I: 2003-02-03)
--Найдите пары моделей PC, имеющих одинаковые скорость и RAM.
--В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i),
--Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и RAM. 
SELECT DISTINCT p.model,
       p1.model,
	   p.speed,
	   p.ram
FROM pc p
JOIN pc p1
ON (p.speed = p1.speed AND p.ram = p1.ram)
WHERE p.model > p1.model


--Задание: 17 (Serge I: 2003-02-03)
--Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК.
--Вывести: type, model, speed
SELECT DISTINCT p.type, l.model, l.speed
FROM product p
JOIN laptop l
ON p.model = l.model
WHERE l.speed < (SELECT MIN(pc.speed)
				 FROM pc)
				 

--Задание: 18 (Serge I: 2003-02-03)
--Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price 
SELECT DISTINCT p.maker, pr.price
FROM product p
JOIN printer pr
ON p.model = pr.model
WHERE pr.price = (SELECT MIN(pr.price)
				  FROM printer pr
				  WHERE pr.color = 'y')
AND pr.color = 'y'


--Задание: 19 (Serge I: 2003-02-13)
--Для каждого производителя, имеющего модели в таблице Laptop,
--найдите средний размер экрана выпускаемых им ПК-блокнотов.
--Вывести: maker, средний размер экрана. 
SELECT p.maker, AVG(l.screen)
FROM product p
JOIN laptop l
ON p.model = l. model
GROUP BY p.maker


--Задание: 20 (Serge I: 2003-02-13)
--Найдите производителей, выпускающих по меньшей мере три различных модели ПК.
--Вывести: Maker, число моделей ПК. 
SELECT p.maker, COUNT(*)
FROM product p
WHERE type = 'pc'
GROUP BY p.maker
HAVING COUNT(*) >= 3


--Задание: 21 (Serge I: 2003-02-13)
--Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
--Вывести: maker, максимальная цена. 
SELECT p.maker, MAX(pc.price)
FROM product p
JOIN pc
ON p.model = pc.model
GROUP BY p.maker
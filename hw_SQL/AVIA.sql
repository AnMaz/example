--1.	Вывести список самолетов с кодами 320, 321, 733;
SELECT *
FROM lanit.aircrafts_data a
WHERE a.aircraft_code IN('320','321','773')


--2.	Вывести список самолетов с кодом не на 3;
SELECT *
FROM lanit.aircrafts_data a
WHERE a.aircraft_code NOT LIKE '3%'


--3.	Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;
SELECT *
FROM lanit.tickets t
WHERE
      t.passenger_name LIKE 'OLGA%'
      AND (t.email LIKE '%olga%'
      OR t.email IS NULL)


--4.	Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;
SELECT *
FROM lanit.aircrafts_data a
WHERE a.range = 5600
OR a.range = 5700
ORDER BY a.range DESC


--5.	Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;
SELECT a.airport_name, a.city
FROM lanit.airports_data a
WHERE a.city LIKE 'Moscow'
ORDER BY a.airport_name


--6.	Вывести список всех городов без повторов в зоне «Europe»;
SELECT DISTINCT a.city
FROM lanit.airports_data a
WHERE a.timezone LIKE 'Europe%'


--7.	Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
SELECT b.book_ref,
	   b.total_amount,
	   b.total_amount * 0.9 AS discounted_amount
FROM lanit.bookings b
WHERE b.book_ref LIKE '3A4%'


--8.	Вывести все данные по местам в самолете с кодом 320 и классом «Business»в формате «Данные по месту: номер места»
SELECT s.aircraft_code
	   || ' '
	   || s.fare_conditions
	   || ': '
	   || s.seat_no AS seat_data
FROM lanit.seats s
WHERE s.aircraft_code = '320'
AND s.fare_conditions = 'Business'


--9.	Найти максимальную и минимальную сумму бронирования в 2017 году;
SELECT MAX(b.total_amount), MIN(b.total_amount)
FROM lanit.bookings b
WHERE TRUNC(b.book_date, 'YEAR') = '01.01.2017'
	
	
--10.	Найти количество мест во всех самолетах;
SELECT s.aircraft_code, COUNT(*)
FROM lanit.seats s
GROUP BY s.aircraft_code


--11.	Найти количество мест во всех самолетах с учетом типа места;
SELECT s.aircraft_code, s.fare_conditions,
       COUNT(*)
FROM lanit.seats s
GROUP BY s.fare_conditions, s.aircraft_code


--12.	Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;
SELECT COUNT(*)
FROM lanit.tickets t
WHERE t.passenger_name LIKE 'ALEKSANDR STEPANOV'
AND t.phone LIKE '%11'


--13.	Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов;
SELECT t.passenger_name,
       COUNT(*)
FROM lanit.tickets t
WHERE t.passenger_name LIKE 'ALEKSANDR %'
GROUP BY t.passenger_name
HAVING COUNT(*) > 2000
ORDER BY COUNT(*) DESC


--14.	Вывести дни в сентябре 2017 с количеством рейсов больше 500.
SELECT TRUNC(f.scheduled_departure, 'dd') AS day,
       COUNT(*)
FROM lanit.flights f
WHERE TRUNC(f.scheduled_departure, 'mm') = '01.09.2017'
GROUP BY TRUNC(f.scheduled_departure, 'dd')
HAVING COUNT(*) > 500


--15.	Вывести список городов, в которых несколько аэропортов
SELECT a.city
FROM lanit.airports_data a
GROUP BY a.city
HAVING COUNT(*) > 1


--16.	Вывести модель самолета и список мест в нем
SELECT a.model,
       LISTAGG(s.seat_no, ', ') WITHIN GROUP (order by s.seat_no) "Seats"
FROM lanit.aircrafts_data a
LEFT JOIN lanit.seats s
ON a.aircraft_code = s.aircraft_code
GROUP BY a.model


--17.	Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017
SELECT *
FROM lanit.airports_data a
JOIN lanit.flights f 
ON a.airport_code = f.departure_airport
WHERE TRUNC(f.scheduled_departure, 'mm') = '01.09.2017'
AND a.city = 'Moscow'


--18.	Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017
SELECT a.airport_code,
       COUNT(*)
FROM lanit.airports_data a
JOIN lanit.flights f 
ON a.airport_code = f.departure_airport
WHERE TRUNC(f.scheduled_departure, 'year') = '01.01.2017'
AND a.city = 'Moscow'
GROUP BY a.airport_code


--19.	Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017
SELECT a.airport_code,
       TRUNC(f.scheduled_departure, 'mm') AS month,
       COUNT(*)
FROM lanit.airports_data a
JOIN lanit.flights f 
ON a.airport_code = f.departure_airport
WHERE TRUNC(f.scheduled_departure, 'year') = '01.01.2017'
AND a.city = 'Moscow'
GROUP BY a.airport_code,
         TRUNC(f.scheduled_departure, 'mm')


--20.	Найти все билеты по бронированию на «3A4B»
SELECT *
FROM lanit.bookings b
WHERE b.book_ref LIKE '3A4B%'


--21.	Найти все перелеты по бронированию на «3A4B»
SELECT *
FROM lanit.flights f
     JOIN lanit.ticket_flights tf
     ON f.flight_id = tf.flight_id
     JOIN lanit.tickets t
     ON tf.ticket_no = t.ticket_no
WHERE t.book_ref LIKE '3A4B%'

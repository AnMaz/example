package steps;

import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.openqa.selenium.By;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class Steps {

    private WebDriver driver;

    @ParameterType(".*")
    public CategoryEnum category(String value) {
        return CategoryEnum.valueOf(value);
    }
    @ParameterType(".*")
    public SortByValueEnum sort(String value) {
        return SortByValueEnum.valueOf(value);
    }

    @Before
    public void setUpDriver(){
        System.setProperty("webdriver.gecko.driver", "C:\\WebDrivers\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        driver = new FirefoxDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Пусть("открыт ресурс авито")
    public void goAvito(){

        driver.get("https://www.avito.ru/");
    }
    @И("в выпадающем списке категорий выбрана {category}")
    public void selectOrgTeh(CategoryEnum category) {
        Select selectCategory = new Select(driver.findElement(By.xpath("//select[@id='category']")));
        selectCategory.selectByVisibleText(category.value);
    }

    @И("в поле поиска введено значение {string}")
    public void writePrinter(String string) {
        WebElement inputText = driver.findElement(By.xpath("//input[@data-marker = 'search-form/suggest']"));
        inputText.sendKeys(string);
    }
    @Тогда("кликнуть по выпадающему списку региона")
    public void clickRegion() {
        WebElement city = driver.findElement(By.xpath("//div[@data-marker='search-form/region']"));
        city.click();
    }
    @Тогда("в поле регион введено значение {string}")
    public void writeVladivostok(String string) throws InterruptedException {
        driver.findElement(By.xpath("//input[@data-marker='popup-location/region/input']"))
                .sendKeys(string);
        Thread.sleep(1000);
    }
    @И("нажата кнопка показать объявления")
    public void clickShowAnnouncements() {
        driver.findElement(By.xpath("//li[@data-marker='suggest(0)']"))
                .click();
        driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']"))
                .click();
    }
    @Тогда("открылась страница результаты по запросу {string}")
    public void pageOpenedRequest(String string) {
        Assert.assertTrue(
                driver.findElement(By.xpath("//h1[@data-marker='page-title/text']"))
                .getText()
                .contains(string)
        );
    }
    @И("активирован чекбокс только с фотографией")
    public void changeCheckbox() {
        WebElement checkbox = driver.findElement(By.xpath("//input[@name='withImagesOnly']/.."));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }
        driver.findElement(By.xpath("//button[@data-marker='search-form/submit-button']"))
                .click();
    }
    @И("в выпадающем списке сортировка выбрано значение {sort}")
    public void selectedMoreExpensive(SortByValueEnum sort) {
        Select sorting = new Select(driver.findElement(By.xpath
                ("//div[@class='sort-select-FaLeL select-select-box-jJiQW select-size-s-VX5kS']" +
                        "/select[@class='select-select-IdfiC']")));
        sorting.selectByVisibleText(sort.value);
    }
    @И("в консоль выведено значение названия и цены {int} первых товаров")
    public void writeInConsole(int number) {
        List<WebElement> printers = driver.findElements(By.xpath("//div[@data-marker='catalog-serp']/div[@id]"));
        for (int i = 0; i<number; i++) {
            System.out.print(i+1 + ". " + printers.get(i).findElement(By.xpath(".//h3")).getText());
            System.out.println(", стоит: " +
                    printers.get(i).findElement(By.xpath
                                    (".//span[@class='price-text-E1Y7h text-text-LurtD text-size-s-BxGpL']"))
                            .getText());
        }
    }
}

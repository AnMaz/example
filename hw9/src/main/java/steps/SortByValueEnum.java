package steps;

public enum SortByValueEnum {
    По_умолчанию("По умолчанию"),
    Дешевле("Дешевле"),
    Дороже("Дороже"),
    По_дате("По дате");

    public String value;
    SortByValueEnum(String value) {
        this.value = value;
    }
}
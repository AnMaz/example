import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/main/cucumber",
        glue = "steps",
        tags = "@1"
)
public class SeleniumTest extends AbstractTestNGCucumberTests {

}

package assistant;
//В этом классе я создаю файл, в котором содержатся все категории на авито,
// в удобном для занесения в ENUM-класс формате, чтобы не вбивать вручную
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class CreateFileCategoryAvito {

    public static void main(String[] args) throws IOException {

        String output;
        FileOutputStream fileOutputStream = new FileOutputStream("D:\\probation\\example\\hw9\\target\\CategoryAvito.txt");

        System.setProperty("webdriver.gecko.driver", "C:\\WebDrivers\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://www.avito.ru/");

        List<WebElement> printers = driver.findElements(By.xpath("//select[@id='category']/option"));
        for (int i = 0; i<printers.size(); i++) {
            output = printers.get(i).getText()
                    .replace(" ", "_").replace(",","")
                    .toLowerCase(Locale.ROOT) +
             " " + "(\"" + printers.get(i).getText() + "\")," + "\n";

            fileOutputStream.write(output.getBytes());
        }

        fileOutputStream.close();
    }
}

import animals.*;
import aviary.*;
import food.*;
import org.apache.log4j.Logger;

public class Zoo {
    private static Logger logger = Logger.getLogger(Zoo.class);

    public static void main(String[] args) {

        Worker worker = new Worker();

        Food grass = new Grass();
        Food meat = new Meat();

        Monkey monkey = new Monkey("monkey");
        Tiger tiger = new Tiger("tiger");
        Beaver beaver1 = new Beaver("beaver Norbert");
        Beaver beaver2 = new Beaver("beaver Daggett");
        Fish fish1 = new Fish("fish Nemo");
        Fish fish2 = new Fish("fish Demo");
        Fish fish3 = new Fish("fish Memo");
        Duck duck = new Duck("duck");
        Giraffe giraffe = new Giraffe("giraffe");

        Aviary<Herbivor> herbivorAviary = new Aviary<>(Size.MEDIUM);
        Aviary<Carnivorous> carnivorousAviary = new Aviary<>(Size.MEDIUM);

        herbivorAviary.addAnimal(beaver2);
        herbivorAviary.addAnimal(monkey);
        herbivorAviary.addAnimal(giraffe);        //giraffe не добавляется в вольер, так как он больше MEDIUM
        herbivorAviary.addAnimal(beaver1);
        herbivorAviary.print();

        herbivorAviary.deleteAnimal(beaver2);
        logger.info("Delete Animal: " + beaver2.getAnimalName());
        herbivorAviary.print();

        carnivorousAviary.addAnimal(fish1);       //fish добавится в вольер, так как она меньше MEDIUM
        carnivorousAviary.addAnimal(duck);        //duck добавится в вольер, так как она MEDIUM
        carnivorousAviary.addAnimal(fish2);
        carnivorousAviary.addAnimal(tiger);       //tiger не добавляется в вольер, так как он больше MEDIUM
        carnivorousAviary.print();

        logger.info("Looking for an animal named");
        System.out.println("Animal link: " + herbivorAviary.getLinkAnimal("beaver Norbert") + "\n");

        //тут рабочий кормит тигра травой и выбрасывает исключение
        System.out.println("Satiety " + tiger.getAnimalName() + " before feeding: " + tiger.getSatiety());
        worker.feed(tiger, grass);
        System.out.println("Satiety " + tiger.getAnimalName() + " after feeding: " + tiger.getSatiety());
    }
}

package exception;

import org.apache.log4j.Logger;

public class WrongFoodException extends Exception {
    private static Logger logger = Logger.getLogger(WrongFoodException.class);
    public WrongFoodException(String message) {
        super(message);
        logger.error("An exception was thrown here", this);
    }
}
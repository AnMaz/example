package aviary;

import animals.Animal;
import animals.Size;

import java.util.HashSet;
import java.util.Objects;

public class Aviary<T> {

    private HashSet<T> hashSet = new HashSet<>();
    private Size sizeOfAviary;

    public Aviary(Size sizeOfAviary) {
        this.sizeOfAviary = sizeOfAviary;
    }

    public void addAnimal(T animal) {
        if (sizeOfAviary.ordinal() >= ((Animal) animal).getSizeOfAnimal().ordinal()) {
            hashSet.add(animal);
        }
    }

    public void deleteAnimal(T animal) {
        hashSet.remove(animal);
    }

    public String getLinkAnimal(String name) {
        for (T obj: hashSet) {
            if (obj.hashCode() == Objects.hash(name)) {
                return obj.getClass().getName() + "@" + Integer.toHexString(obj.hashCode());
            }
        }
        return "The animal is absent!";
    }

    public void print() {
        System.out.print("Animals in the aviary: ");
        for (T obj: hashSet) {
            System.out.print(obj.toString() + ", ");
        }
        System.out.println("\n");
    }
}
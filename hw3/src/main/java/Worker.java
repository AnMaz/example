import animals.Animal;
import animals.Voice;
import exception.WrongFoodException;
import food.Food;

public class Worker {

    public void feed(Animal animal, Food food) {
        try {
            if (animal.eat(food)) {
                System.out.println(("The " + animal.getAnimalName() + " has eaten " + food.getFoodName()) + "!");
            }
        } catch (WrongFoodException e) {
            e.printStackTrace();
            System.out.println(("The " + animal.getAnimalName() + " hasn't eaten " + food.getFoodName()) + "!");
        }
    }

    public String getVoice(Voice animal) {
        return animal.voice();
    }
}
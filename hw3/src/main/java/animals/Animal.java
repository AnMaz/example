package animals;

import exception.WrongFoodException;
import food.Food;

import java.util.Objects;

public abstract class Animal {

    private int satiety = 0;
    protected String name;
    private Size sizeOfAnimal;

    protected void setSatiety(Food food) {
        satiety += food.theNutritionalValue();
    }

    public int getSatiety() {
        return satiety;
    }

    protected void setSizeOfAnimal(Size sizeOfAnimal) {
        this.sizeOfAnimal = sizeOfAnimal;
    }

    public Size getSizeOfAnimal() {
        return sizeOfAnimal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }

    public abstract boolean eat(Food food) throws WrongFoodException;

    public String getAnimalName() {
        return name;
    }
}

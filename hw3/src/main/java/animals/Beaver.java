package animals;

public class Beaver extends Herbivor implements Voice, Swim {

    public Beaver (String name) {
        this.name = name;
        setSizeOfAnimal(Size.MEDIUM);
    }

    @Override
    public void swim() {
        System.out.println("swim");
    }

    @Override
    public String voice() {
        return "I'm a beaver!";
    }

}

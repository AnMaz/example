package animals;

public class Monkey extends Herbivor implements  Voice, Run {

    public Monkey (String name) {
        this.name = name;
        setSizeOfAnimal(Size.MEDIUM);
    }

    @Override
    public void run() {
        System.out.println("run");
    }

    @Override
    public String voice() {
        return "U-A-A U-A-A";
    }

}
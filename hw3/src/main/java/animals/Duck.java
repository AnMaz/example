package animals;

public class Duck extends Carnivorous implements Voice, Swim, Fly {

    public Duck (String name) {
        this.name = name;
        setSizeOfAnimal(Size.MEDIUM);
    }

    @Override
    public void fly() {
        System.out.println("fly");
    }

    @Override
    public void swim() {
        System.out.println("swim");
    }

    @Override
    public String voice() {
        return "KRYA KRYA";
    }

}

package animals;

public class Giraffe extends Herbivor implements Voice, Run {

    public Giraffe (String name) {
        this.name = name;
        setSizeOfAnimal(Size.HUGE);
    }

    @Override
    public void run() {
        System.out.println("run");
    }

    @Override
    public String voice() {
        return "MMMAAA";
    }

}

package animals;

import food.Food;
import food.Meat;
import exception.WrongFoodException;

public abstract class Carnivorous extends Animal {

    @Override
    public boolean eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            setSatiety(food);
            System.out.println("This food is suitable!");
            return true;
        }
        else {
            throw new WrongFoodException("The carnivorous doesn't eat grass!");
        }
    }
}
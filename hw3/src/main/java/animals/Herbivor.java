package animals;

import exception.WrongFoodException;
import food.Food;
import food.Grass;

public abstract class Herbivor extends Animal {

    @Override
    public boolean eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            setSatiety(food);
            System.out.println("This food is suitable!");
            return true;
        }
        else {
            throw new WrongFoodException("The herbivore doesn't eat meat!");
        }
    }
}
import calculatorLogic.CalculatorLogic;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorDifferenceTest {

    @DataProvider
    public Object[][] dataDifference() {
        return new Object[][] {
                {0, 0, 0},
                {-1, 0, 1},
                {-6, -2, 4},
                {10, 13, 3},
                {100, 25, -75}
        };
    }

    @DataProvider
    public Object[][] dataNotDifference() {
        return new Object[][] {
                {10, 0, 0},
                {1, 5, 1},
                {-3, 6, 3},
                {19, 3, 7}
        };
    }

    @Test(dataProvider = "dataDifference")
    public void differenceTest(int difResult, int one, int two) {
        Assert.assertEquals(difResult, CalculatorLogic.getDifference(one, two),"Значения не корректны!");
    }

    @Test(dataProvider = "dataNotDifference")
    public void notDifferenceTest(int difResult, int one, int two) {
        Assert.assertNotEquals(difResult, CalculatorLogic.getDifference(one, two),"Значения не корректны!");
    }
}

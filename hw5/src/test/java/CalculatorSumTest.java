import calculatorLogic.CalculatorLogic;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class CalculatorSumTest {

    @DataProvider
    public Object[][] dataSum() {
        return new Object[][] {
            {0, 0, 0},
            {1, 0, 1},
            {-3, -6, 3},
            {-3, 3, -6},
            {10, 3, 7}
        };
    }

    @DataProvider
    public Object[][] dataNotSum() {
        return new Object[][] {
                {10, 0, 0},
                {1, 5, 1},
                {-3, 6, 3},
                {19, -3, 7},
                {53, 10, -36}
        };
    }

    @Test(dataProvider = "dataSum")
    public void sumTest(int sum, int one, int two) {
        Assert.assertEquals(sum, CalculatorLogic.getSum(one, two),"Значения не корректны!");
    }

    @Test(dataProvider = "dataNotSum")
    public void notSumTest(int sum, int one, int two) {
        Assert.assertNotEquals(sum, CalculatorLogic.getSum(one, two),"Значения не корректны!");
    }
}

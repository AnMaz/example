import calculatorLogic.CalculatorLogic;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorMultiplicationTest {

    @DataProvider
    public Object[][] dataMultiplication() {
        return new Object[][] {
                {0, 0, 0},
                {0, 0, 1},
                {-6, -2, 3},
                {10, -2, -5},
                {4, 2, 2}
        };
    }

    @DataProvider
    public Object[][] dataNotMultiplication() {
        return new Object[][] {
                {10, 0, 0},
                {1, 5, 1},
                {-3, 6, 3},
                {-19, -3, 7}
        };
    }

    @Test(dataProvider = "dataMultiplication")
    public void multiplicationTest(int multResult, int one, int two) {
        Assert.assertEquals(multResult, CalculatorLogic.getMultiplication(one, two),"Значения не корректны!");
    }

    @Test(dataProvider = "dataNotMultiplication")
    public void notMultiplicationTest(int multResult, int one, int two) {
        Assert.assertNotEquals(multResult, CalculatorLogic.getMultiplication(one, two),"Значения не корректны!");
    }
}

import calculatorLogic.CalculatorLogic;
import calculatorLogic.DivideException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorDivideTest {

    @DataProvider
    public Object[][] dataDivide() {
        return new Object[][] {
                {0, 0, 6},
                {-1, -1, 1},
                {10, 40, 4},
                {2.5, 5, 2},
                {-2.5, 5, -2},
                {4, -8, -2}
        };
    }

    @DataProvider
    public Object[][] dataNotDivide() {
        return new Object[][] {
                {1, 0, 1},
                {1, 5, 1},
                {-2, 6, 3}
        };
    }

    @DataProvider
    public Object[][] dataDivideByZero() {
        return new Object[][] {
                {0, 0},
                {5, 0},
                {-6, 0}
        };
    }

    @Test(dataProvider = "dataDivide")
    public void divideTest(double divResult, int one, int two) throws DivideException {
        Assert.assertEquals(divResult, CalculatorLogic.getDivide(one, two),"Значения не корректны!");
    }

    @Test(dataProvider = "dataNotDivide")
    public void notDivideTest(double divResult, int one, int two) throws DivideException {
        Assert.assertNotEquals(divResult, CalculatorLogic.getDivide(one, two),"Значения не корректны!");
    }

    @Test(dataProvider = "dataDivideByZero", expectedExceptions = DivideException.class)
    public void divideByZeroTest(int one, int two) throws DivideException {
        CalculatorLogic.getDivide(one, two);
        Assert.fail("Значение не ноль!");
    }
}

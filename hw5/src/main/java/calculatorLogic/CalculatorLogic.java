package calculatorLogic;

public class CalculatorLogic {

    public static int getSum(int one, int two) {
        return one + two;
    }

    public static int getMultiplication(int one, int two) {
        return one * two;
    }

    public static int getDifference (int one, int two) {
        return one - two;
    }

    public static double getDivide (int one, int two) throws DivideException {
        if (two != 0) {
            return (double)one / (double)two;
        }
        throw new DivideException("На ноль делить нельзя!");
    }
}

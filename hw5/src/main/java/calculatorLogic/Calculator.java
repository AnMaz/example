package calculatorLogic;

import java.util.Scanner;

public class Calculator {

    private static int num;
    private static String message;
    private static String whatToDo;
    private static final Scanner in = new Scanner(System.in);

    public static int getNum() {
        try {
            num = Integer.parseInt(in.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Некорректное значение, введите целое число!");
            Calculator.getNum();
        }
        return num;
    }

    public static void getMessage() {
        System.out.println("Введите два числа которые надо " + message);
    }
    public static void getResult() {
        System.out.println("Результат вычисления: " + whatToDo);
    }

    public static void startCalculator() {
        System.out.println("Введите желаемое действие:" + "\n" + "-Сложить" + "\n" + "-Вычесть"
                + "\n" + "-Умножить" + "\n" + "-Разделить");
        message = in.nextLine().toLowerCase();
        switch (message) {
            case ("сложить"):
                Calculator.getMessage();
                whatToDo = String.valueOf(CalculatorLogic.getSum(Calculator.getNum(), Calculator.getNum()));
                Calculator.getResult();
                break;
            case ("вычесть"):
                Calculator.getMessage();
                whatToDo = String.valueOf(CalculatorLogic.getDifference(Calculator.getNum(), Calculator.getNum()));
                Calculator.getResult();
                break;
            case ("умножить"):
                Calculator.getMessage();
                whatToDo = String.valueOf(CalculatorLogic.getMultiplication(Calculator.getNum(), Calculator.getNum()));
                Calculator.getResult();
                break;
            case ("разделить"):
                Calculator.getMessage();
                try {
                    whatToDo = String.valueOf(CalculatorLogic.getDivide(Calculator.getNum(), Calculator.getNum()));
                    Calculator.getResult();
                } catch (DivideException e) {
                    System.out.println("На ноль делить нельзя!");
                }
                break;
            default:
                System.out.println("Некорректный ввод, попробуйте снова!" + "\n");
                Calculator.startCalculator();
                break;
        }
    }
}

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.List;
import java.util.concurrent.TimeUnit;

public class SeleniumRequest {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "C:\\WebDrivers\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        WebDriverWait wait = new WebDriverWait(driver,10);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://www.avito.ru/");

        Select category = new Select(driver.findElement(By.xpath("//select[@id='category']")));
        category.selectByVisibleText("Оргтехника и расходники");

        WebElement inputText = driver.findElement(By.xpath("//input[@data-marker = 'search-form/suggest']"));
        inputText.sendKeys("Принтер");

        WebElement city = driver.findElement(By.xpath("//div[@data-marker='search-form/region']"));
        city.click();
        driver.findElement(By.xpath("//input[@data-marker='popup-location/region/input']"))
                .sendKeys("Владивосток");
        wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//li[@data-marker='suggest(0)']")),
                        "Владивосток"));
        driver.findElement(By.xpath("//li[@data-marker='suggest(0)']"))
                .click();
        driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']"))
                .click();

        WebElement checkbox = driver.findElement(By.xpath("//span[@data-marker='delivery-filter/text']"));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }
        driver.findElement(By.xpath("//button[@data-marker='search-filters/submit-button']"))
                .click();

        Select sorting = new Select(driver.findElement(By.xpath("//*[text()='Дороже']/..")));
        sorting.selectByVisibleText("Дороже");

        List<WebElement> printers = driver.findElements(By.xpath("//div[@data-marker='catalog-serp']/div[@id]"));
        for (int i = 0; i<3; i++) {
            System.out.print(i+1 + ". " + printers.get(i).findElement(By.xpath(".//h3")).getText());
            System.out.println(", стоит: " +
                    printers.get(i).findElement(By.xpath
                            (".//span[@class='price-text-E1Y7h text-text-LurtD text-size-s-BxGpL']"))
                            .getText());
        }
    }
}

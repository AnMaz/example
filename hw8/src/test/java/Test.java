public class Test {
    @org.testng.annotations.Test
    public void startTest() throws InterruptedException {
        SeleniumRequest.setUpDriver();
        SeleniumRequest.goAvito("https://www.avito.ru/");
        SeleniumRequest.selectOrgTeh("Оргтехника и расходники");
        SeleniumRequest.writePrinter();
        SeleniumRequest.clickRegion();
        SeleniumRequest.writeVladivostok("Владивосток");
        SeleniumRequest.clickShowAnnouncements();
        SeleniumRequest.pageOpenedRequest("Принтер");
        SeleniumRequest.changeCheckbox();
        SeleniumRequest.selectedMoreExpensive("Дороже");
        SeleniumRequest.writeInConsole();

    }
}

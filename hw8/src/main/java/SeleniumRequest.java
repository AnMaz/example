import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SeleniumRequest {
    private static WebDriver driver;
    private static boolean bool;

    @Step("Запускаем браузер")
    public static void setUpDriver(){
        System.setProperty("webdriver.gecko.driver", "C:\\WebDrivers\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        driver = new FirefoxDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Step("Вводим адрес сайта")
    public static void goAvito(String string){
        driver.get(string);
        Assert.assertEquals(driver.getCurrentUrl(), string, "Открылась не та ссылка!");
    }
    @Step("Выбираем категорию")
    public static void selectOrgTeh(String string) {
        Select selectCategory = new Select(driver.findElement(By.xpath("//select[@id='category']")));
        selectCategory.selectByVisibleText(string);
        Assert.assertEquals(string,
                driver.findElement(By.xpath("//select[@id='category']/option[@selected]")).getText(),
                "Выбран не тот элемент!");
    }

    @Step("Вводим значение в поисковое окно")
    public static void writePrinter() {
        try {
            WebElement inputText = driver.findElement(By.xpath("//input[@data-marker = 'search-form/suggest']"));
            inputText.sendKeys("Принтер");
            bool = true;
        } catch (NoSuchElementException e) {
            System.out.println("Поле для ввода не найдено!");
            bool = false;
        }
        Assert.assertTrue(bool);
    }
    @Step("Начинаем выбирать регион")
    public static void clickRegion() {
        try {
            WebElement city = driver.findElement(By.xpath("//div[@data-marker='search-form/region']"));
            city.click();
            bool = true;
        } catch (NoSuchElementException e) {
            System.out.println("Чекбокс не найден!");
            bool = false;
        }
        Assert.assertTrue(bool);
    }
    @Step("Вводим регион")
    public static void writeVladivostok(String string) throws InterruptedException {
        driver.findElement(By.xpath("//input[@data-marker='popup-location/region/input']"))
                .sendKeys(string);
        Thread.sleep(1000);
        Assert.assertTrue(driver.findElement(By.xpath("//li[@data-marker='suggest(0)']"))
                .getText().contains(string), "Не тот регион!");
    }
    @Step("Смотрим результаты")
    public static void clickShowAnnouncements() {
        try {
            driver.findElement(By.xpath("//li[@data-marker='suggest(0)']"))
                    .click();
            driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']"))
                    .click();
            bool = true;
        } catch (NoSuchElementException e) {
            System.out.println("Элемент не найден!");
            bool = false;
        }
        Assert.assertTrue(bool);
    }
    @Step("Проверяем то ли нам выдало")
    public static void pageOpenedRequest(String string) {
        Assert.assertTrue(
                driver.findElement(By.xpath("//h1[@data-marker='page-title/text']"))
                        .getText()
                        .contains(string)
        );
    }
    @Step("Ставим чекбокс на доставку")
    public static void changeCheckbox() {
        try {
            ((JavascriptExecutor) driver).executeScript("scroll(0,300)");
            WebElement checkbox = driver.findElement(By.xpath("//span[@data-marker='delivery-filter/text']"));
            if (!checkbox.isSelected()) {
                checkbox.click();
            }
            driver.findElement(By.xpath("//button[@data-marker='search-filters/submit-button']"))
                    .click();
            bool = true;
        } catch (NoSuchElementException e) {
            System.out.println("Элемент не найден!");
            bool = false;
        }
        Assert.assertTrue(bool);
    }
    @Step("Сортируем результаты")
    public static void selectedMoreExpensive(String string) {
        Select sorting = new Select(driver.findElement(By.xpath
                ("//div[@class='sort-select-FaLeL select-select-box-jJiQW select-size-s-VX5kS']" +
                        "/select[@class='select-select-IdfiC']")));
        sorting.selectByVisibleText(string);
        Assert.assertEquals(string,
                driver.findElement(By.xpath("//div[@class='sort-select-FaLeL select-select-box-jJiQW select-size-s-VX5kS']" +
                        "/select[@class='select-select-IdfiC']" +
                        "/option[@selected]")).getText(),
                "Выбран не тот элемент!");
    }
    @Step("Выводим в консоль")
    public static void writeInConsole() {
        try {
            List<WebElement> printers = driver.findElements(By.xpath("//div[@data-marker='catalog-serp']/div[@id]"));
            for (int i = 0; i<3; i++) {
                System.out.print(i+1 + ". " + printers.get(i).findElement(By.xpath(".//h3")).getText());
                System.out.println(", стоит: " +
                        printers.get(i).findElement(By.xpath
                                        (".//span[@class='price-text-E1Y7h text-text-LurtD text-size-s-BxGpL']"))
                                .getText());
            }
            bool = true;
        } catch (NoSuchElementException e) {
            System.out.println("Ошибка вывода!");
            bool = false;
        }
        Assert.assertTrue(bool);
    }
}

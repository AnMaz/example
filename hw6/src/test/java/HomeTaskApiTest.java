import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.Random;

import static io.restassured.RestAssured.given;

public class HomeTaskApiTest {

    Order order = new Order();
    int id = new Random().nextInt(9)+1;
    int petId = new Random().nextInt(500000);
    {
        order.setId(id);
        order.setPetId(petId);
    }

    @BeforeClass
    public void prepare() {

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://213.239.217.15:9090/api/v3/")
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        RestAssured.filters(new ResponseLoggingFilter());
    }

    @Test
    public void checkObjectSave() {

        given()
                .body(order)
                .when()
                .post("/store/order")
                .then()
                .statusCode(200);

        Order actual =
                given()
                        .pathParam("orderId", id)
                        .when()
                        .get("/store/order/{orderId}")
                        .then()
                        .statusCode(200)
                        .extract().body()
                        .as(Order.class);

        Assert.assertEquals(actual.getPetId(), order.getPetId());
    }

    @Test
    public void checkObjectDelete() {
        given()
                .body(order)
                .when()
                .post("/store/order")
                .then()
                .statusCode(200);
        given()
                .pathParam("orderId", id)
                .when()
                .delete("/store/order/{orderId}")
                .then()
                .statusCode(200);
        given()
                .pathParam("orderId", id)
                .when()
                .get("/store/order/{orderId}")
                .then()
                .statusCode(404);
    }

    @Test
    public void checkInventoryValue() {
        Map inventory =
            given()
                    .when()
                    .get("/store/inventory ")
                    .then()
                    .statusCode(200)
                    .extract().body()
                    .as(Map.class);
        Assert.assertTrue(inventory.containsKey("sold"), "Inventory не содержит статус sold" );
    }
}
